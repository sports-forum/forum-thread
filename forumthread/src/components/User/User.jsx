import './User.css';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import { userRole } from '../../common/user-role';
import { Button, Col, Row } from 'react-bootstrap';

/**
 * 
 * @param {{ tweet: { id: string, content: string, author: string, date: Date, likedBy: string[] } } } tweet 
 */

const User = ({currentUserData, blockUser, unblockUser, makeAdmin}) => {
    const { userData: { handle, role} } = useContext(AppContext);

    const isUserBlocked = () => {
        return currentUserData.role === userRole.BLOCKED;
    }

    return (
    <div className='User'>
		<Row>
			<Col>
			{currentUserData.handle === handle && '>>> '}
        {currentUserData.handle} &nbsp;
        {'[' + Object.keys(userRole)[currentUserData.role] + ']'}
			</Col>
			<Col className='text-right'>
			{(currentUserData.role < userRole.ADMIN || role === userRole.OWNER) && <><Button onClick={isUserBlocked() ? () => unblockUser(currentUserData.handle) : () => {
            if(role < currentUserData.role){
                alert('This user has a higher role, cannot be blocked');
                return;
            }
            blockUser(currentUserData.handle);
        }} className={ isUserBlocked() ? 'btn-success' : 'btn-danger'}>{isUserBlocked() ? 'Unblock' : 'Block'}</Button> </>}
        {(currentUserData.role < userRole.ADMIN && role === userRole.OWNER) && <Button onClick={() => makeAdmin(currentUserData.handle)} className="btn-info ml-3">Make admin</Button>}
			</Col>
		</Row>
        
    </div>
    )
};

export default User;