import { useContext, useEffect, useState } from 'react';
import AppContext from '../providers/AppContext';
import { getTweetsByAuthor, getLikedTweets } from '../services/tweets.service';
import { logoutUser } from "../services/auth.service";
import { useNavigate } from 'react-router-dom';
import { Button } from "react-bootstrap";

const LogoutComponent = ({set}) => {
    const navigate = useNavigate();

	const { user, userData, setContext } = useContext(AppContext);
	const handle = userData.handle;
	const [tweets, setTweets] = useState({ liked: [], own: [] });
  
	useEffect(() => {
	  Promise.all([getTweetsByAuthor(handle), getLikedTweets(handle)])
		.then(([own, liked]) => {
		  setTweets({ own, liked });
		})
		.catch(console.error);
	}, [handle]);

    const logout = () => {
    
        logoutUser()
        .then(() => {
            set({ user: null, userData: {handle: null} });
        });
        navigate('/')
    };

    return (
        <div className="text-white mt-3 mb-3">
			<h4>{ user.displayName ? 'Hello, ' + user.displayName : user.email }</h4>
			<p>Posts by you, count: <strong>{tweets.own.length}</strong></p>
			<p>Posts you have liked, count: <strong>{tweets.liked.length}</strong></p>

			<Button onClick={logout}>Logout</Button>
        </div>
    );
}

export default LogoutComponent;