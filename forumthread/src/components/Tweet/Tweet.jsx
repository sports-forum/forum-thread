import './Tweet.css';
import { useContext, useState } from 'react';
import AppContext from '../../providers/AppContext';
import { userRole } from '../../common/user-role';
import { Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom'

/**
 * 
 * @param {{ tweet: { id: string, content: string, author: string, date: Date, likedBy: string[] } } } tweet 
 */

const Tweet = ({ deleteTweet, tweet, like, dislike, edit }) => {
	const { userData: { handle, role } } = useContext(AppContext);

	const [content, setContent] = useState('');
	const [title, setTitle] = useState('');
	const [isPostEdited, setIsPostEdited] = useState(true);
	const navigate = useNavigate();

	const isTweetLiked = () => tweet.likedBy.includes(handle);


	return (
		<Card className='mt-4'>
			{/*<Card.Img variant="top" src="holder.js/100px180" />*/}
			<Card.Body className="p-0">
				<Card.Title className="pt-3 pl-3 pr-3 pb-3 bg-primary">{isPostEdited ? (tweet.title ? tweet.title : 'Untitled') :
					<Form.Group className="form-group">
						<Form.Label htmlFor='title' className='h6 '>Title: </Form.Label>
						<Form.Control id="title" type="text" value={title} required onChange={e => setTitle(e.target.value)} />
					</Form.Group>}
				</Card.Title>
				<Card.Subtitle className="mb-2 pl-3 pr-3 text-info ">{tweet.author}</Card.Subtitle>
				<Card.Text className='mt-3 pl-3 pr-3'>{isPostEdited ? (tweet.content) :
					<Form.Group className="form-group">
						<Form.Label htmlFor='content' className='h6'>Comment: </Form.Label>
						<Form.Control as="textarea" rows={3} id="content" value={content} required onChange={e => setContent(e.target.value)} />
					</Form.Group>}
				</Card.Text>
				<Card.Footer className="text-muted">
					<Row>
						<Col>
							<span>Likes: {tweet.likedBy.length}</span>
							{(handle !== null && role >= userRole.BASIC) && <Button onClick={isTweetLiked() ? () => dislike(handle, tweet) : () => like(handle, tweet)} className="btn-sm ml-2">{isTweetLiked() ? 'Dislike' : 'Like'}</Button>}
							{handle !== null && <Button onClick={() => { navigate(`/comments/${tweet.id}`) }} className="btn-sm ml-2">Reply</Button>}
						</Col>
						<Col className='justify-content-end text-right'>
							{(tweet.author === handle && role >= userRole.BASIC) && (isPostEdited ? (<Button onClick={() => { setContent(tweet.content); setTitle(tweet.title); setIsPostEdited(false) }} className='btn-success btn-sm mr-3'>Edit</Button>) : (<Button onClick={() => { edit(tweet, content, title); setIsPostEdited(true); }} className='btn-sm mr-3'>Save</Button>))}
							{tweet.author === handle || role >= userRole.ADMIN ? <Button onClick={() => deleteTweet(tweet.id, navigate)} className='btn-danger btn-sm'>Delete</Button> : ''}
						</Col>
					</Row>
				</Card.Footer>
			</Card.Body>
		</Card>
	)
};

export default Tweet;