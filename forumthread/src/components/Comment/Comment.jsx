import './Comment.css';
import { useContext, useState } from 'react';
import AppContext from '../../providers/AppContext';
import { userRole } from '../../common/user-role';
import { Card, Button, Row, Col, Form } from 'react-bootstrap';


const Comment = ({ deleteComment, comment, edit }) => {
	const { userData: { handle, role } } = useContext(AppContext);

	const [content, setContent] = useState('');
	const [title, setTitle] = useState('');
	const [isPostEdited, setIsPostEdited] = useState(true);

	return (
		<Card className='mt-4'>
			{/*<Card.Img variant="top" src="holder.js/100px180" />*/}
			<Card.Body className="p-0">
				<Card.Subtitle className={'mb-2 pl-3 pt-3 text-muted'}>{comment.author}</Card.Subtitle>
				<Card.Text className='mt-3 pl-3 pr-3'>{isPostEdited ? (comment.content) :
					<Form.Group className="form-group">
						<Form.Label htmlFor='content' className='h6'>Comment: </Form.Label>
						<Form.Control as="textarea" rows={3} id="content" value={content} required onChange={e => setContent(e.target.value)} />
					</Form.Group>}
				</Card.Text>
				<Card.Footer className="text-muted">
					<Row>
						<Col className='justify-content-end text-right'>
							{comment.author === handle && (isPostEdited ? (<Button onClick={() => { setContent(comment.content); setTitle(comment.title); setIsPostEdited(false) }} className='btn-success btn-sm mr-3'>Edit</Button>) : (<Button onClick={() => { edit(content, comment.id); setIsPostEdited(true); }} className='btn-sm mr-3'>Save</Button>))}
							{comment.author === handle || role >= userRole.ADMIN ? <Button onClick={() => deleteComment(comment.id)} className='btn-danger btn-sm'>Delete</Button> : ''}
						</Col>
					</Row>
				</Card.Footer>
			</Card.Body>
		</Card>
	)
};

export default Comment;