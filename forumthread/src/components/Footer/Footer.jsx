import { Col, Row } from 'react-bootstrap';
import './Footer.css';

const Footer = () => {

  return (
	  <footer className='container-fluid Footer'>
	    <Row>
		  <Col>&copy;2022 </Col>
	    </Row>
	  </footer>
  )
};

export default Footer;
