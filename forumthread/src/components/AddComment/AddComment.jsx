import { useState } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import './AddComment.css';

/**
 * 
 * @param {{ onSubmit: (content: string) => Promise<any>}} param0 
 * @returns 
 */
const AddComment = ({ onSubmit }) => {
  const [content, setContent] = useState('');

  const addComment = () => {
    onSubmit(content)
      .then(() => {
        setContent('');
        alert('Comment added successfully!');
      })
      .catch(e => alert(e.message));
  };

  return (
<Card className='mt-4'>
	<Card.Header className='bg-info text-white h4'>Add a comment</Card.Header>
  <Card.Body>
  	<Form>
          <Form.Group className="form-group">
            <Form.Label htmlFor='content'>Content: </Form.Label>
			<Form.Control as="textarea" rows={3} id="content" value={content} required onChange={e => setContent(e.target.value)} />
          </Form.Group>
          <Button onClick={addComment}>Post</Button>
	</Form>
  </Card.Body>
  </Card>
  );
};

export default AddComment;