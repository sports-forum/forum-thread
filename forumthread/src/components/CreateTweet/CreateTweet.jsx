import { useState } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import './CreateTweet.css';

/**
 * 
 * @param {{ onSubmit: (content: string) => Promise<any>}} param0 
 * @returns 
 */
const CreateTweet = ({ onSubmit }) => {
  const [content, setContent] = useState('');
  const [title, setTitle] = useState('');

  const createTweet = () => {
    onSubmit(content, title)
      .then(() => {
        setContent('');
        setTitle('');
        alert('Tweet created!');
      })
      .catch(e => alert(e.message));
  };

  return (
<Card className='mt-4'>
	<Card.Header className='bg-info text-white h4'>Create new post</Card.Header>
  <Card.Body>
  	<Form>
		  <Form.Group className="form-group">
			<Form.Label htmlFor='title'>Title: </Form.Label>
            <Form.Control id="title" type="text" value={title} required onChange={e => setTitle(e.target.value)} />
          </Form.Group>
          <Form.Group className="form-group">
            <Form.Label htmlFor='content'>Comment: </Form.Label>
			<Form.Control as="textarea" rows={3} id="content" value={content} required onChange={e => setContent(e.target.value)} />
          </Form.Group>
          <Button onClick={createTweet}>Post</Button>
	</Form>
  </Card.Body>
  </Card>
  );
};

export default CreateTweet;