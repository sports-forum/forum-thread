import { useContext } from 'react';
import AppContext from '../providers/AppContext';
import { useLocation, Navigate } from 'react-router-dom';

export default function Authenticated ({ children, loading }) { //  /tweets
  const { user } = useContext(AppContext);
  const location = useLocation();

  if (loading) {
    return <h1>Loading...</h1>
  }
  
  if (!user && location.pathname === '/profile') {
    return <Navigate to="/home" state={{ from: location }} />
  }
  

  return children;
}
