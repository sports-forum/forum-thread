import { Link, Route, useNavigate, useRouter } from 'react-router-dom';
import { BrowserRouter, Routes } from 'react-router-dom';
import './App.css';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Home from './views/Home/Home';
import Login from './views/Login/Login';
import Register from './views/Register/Register';
import Tweets from './views/Tweets/Tweets';
import { useState, useEffect, useContext} from 'react';
import AppContext  from './providers/AppContext';
import { logoutUser } from './services/auth.service';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './config/firebase-config';
import { getUserData } from './services/users.services';
import Authenticated from './hoc/Authenticated';
import Profile from './views/Profile/Profile';
import { userRole } from './common/user-role';
import Users from './views/Users/Users';
import LogoutComponent from './components/LogoutComponent';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Container, Row } from 'react-bootstrap';
import Post from './views/Post/Post';
import { deleteTweet, dislikeTweet, editPost, likeTweet } from './services/tweets.service';



function App() {
  const [appState, setAppState] = useState({
    user: null,
    userData: { handle: null },
  });


  let [user, loading, error] = useAuthState(auth);

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then(snapshot => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        setAppState({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });


      })
      .catch(e => alert(e.message));
  }, [user]);

  const like = (handle,tweet) => {
    likeTweet(handle, tweet.id)
      .then(() => {
        // setTweets(tweets.map(t => {
        //   if (t.id === tweet.id) {
        //     t.likedBy.push(handle);
        //   }

        //   return t;
        // }));
      });
  };

  const dislike = (handle,tweet) => {
    dislikeTweet(handle, tweet.id)
      .then(() => {
        // setTweets(tweets.map(t => {
        //   if (t.id === tweet.id) {
        //     t.likedBy = t.likedBy.filter(h => h !== handle);
        //   }

        //   return t;
        // }));
      });
  };

  const delTweet = (id, nav) => {
    deleteTweet(id)
      .then(() => {
        // setTweets(tweets.filter(t => t.id !== id));
      })
      .catch(console.error);
    nav('/');
  };

  const edit = (tweet, content, title) => {
    editPost(tweet.id, content, title)
      .then(() => {})
      .catch(console.error);
  }



  return (
    <BrowserRouter>
      <AppContext.Provider value={{ ...appState, setContext: setAppState }}>

        <Header loading={loading} appState={appState} userRole={userRole} />
        <Container fluid>
          <div style={{ textAlign: 'center' }}>
            {loading
              ? <p>Loading user data</p>
              : null
            }
          </div>
          <Row>
            <Col className='bg-light'>
              <Routes>
                <Route index element={<Home />} />
                <Route path='home' element={<Home />} />
                <Route path='register' element={<Home />} />
                <Route path='login' element={<Home />} />
                <Route exact path='comments/:postId' element={<Post like = {like} dislike = {dislike} edit = {edit} delTweet = {delTweet}  />} />
                <Route path='users' element={<Users />} />
                <Route path='tweets' element={<Authenticated loading={loading}><Tweets like = {like} dislike = {dislike} edit = {edit} delTweet = {delTweet} /></Authenticated>} />
              </Routes>
            </Col>
            <Col sm="3" xs="4" className='bg-secondary'>
              <Routes>
                <Route index element={appState.user === null ? <Login /> : <LogoutComponent set={setAppState} />} />
                <Route path='home' element={appState.user === null ? <Login /> : <LogoutComponent set={setAppState} />} />
                <Route path='register' element={appState.user === null ? <Register /> : <LogoutComponent set={setAppState} />} />
                <Route path='login' element={appState.user === null ? <Login /> : <LogoutComponent set={setAppState} />} />
                <Route path='users' element={appState.user === null ? <Login /> : <LogoutComponent set={setAppState} />} />
                <Route path='tweets' element={appState.user === null ? <Login /> : <LogoutComponent set={setAppState} />} />
              </Routes>
            </Col>
          </Row>
        </Container>
        <Footer />
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
