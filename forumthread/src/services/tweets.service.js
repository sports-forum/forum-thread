import {
  ref,
  push,
  get,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
} from "firebase/database";
import { db } from "../config/firebase-config";

export const fromTweetsDocument = (snapshot) => {
  const tweetsDocument = snapshot.val();

  return Object.keys(tweetsDocument).map((key) => {
    const tweet = tweetsDocument[key];

    return {
      ...tweet,
      id: key,
      createdOn: new Date(tweet.createdOn),
      likedBy: tweet.likedBy ? Object.keys(tweet.likedBy) : [],
    };
  });
};

export const addTweet = (content, title, handle) => {
  return push(ref(db, "tweets"), {
    content,
    title: title,
    author: handle,
    createdOn: Date.now(),
  }).then((result) => {
    return getTweetById(result.key);
  });
};

export const getTweetById = (id) => {
  return get(ref(db, `tweets/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Tweet with id ${id} does not exist!`);
    }

    const tweet = result.val();
    tweet.id = id;
    tweet.createdOn = new Date(tweet.createdOn);
    if (!tweet.likedBy) {
      tweet.likedBy = [];
    } else {
      tweet.likedBy = Object.keys(tweet.likedBy);
    }

    return tweet;
  });
};

export const getLikedTweets = (handle) => {
  return get(ref(db, `users/${handle}`)).then((snapshot) => {
    if (!snapshot.val()) {
      throw new Error(`User with handle @${handle} does not exist!`);
    }

    const user = snapshot.val();
    if (!user.likedTweets) return [];

    return Promise.all(
      Object.keys(user.likedTweets).map((key) => {
        return get(ref(db, `tweets/${key}`)).then((snapshot) => {
          const tweet = snapshot.val();

          return {
            ...tweet,
            createdOn: new Date(tweet.createdOn),
            id: key,
            likedBy: tweet.likedBy ? Object.keys(tweet.likedBy) : [],
          };
        });
      })
    );
  });
};

export const getTweetsByAuthor = (handle) => {
  return get(
    query(ref(db, "tweets"), orderByChild("author"), equalTo(handle))
  ).then((snapshot) => {
    if (!snapshot.exists()) return [];

    return fromTweetsDocument(snapshot);
  });
};

export const getLiveTweets = (listen) => {
  return onValue(ref(db, "tweets"), listen);
};

export const getAllTweets = () => {
  return get(ref(db, "tweets")).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromTweetsDocument(snapshot);
  });
};

export const likeTweet = (handle, tweetId) => {
  const updateLikes = {};
  updateLikes[`/tweets/${tweetId}/likedBy/${handle}`] = true;
  updateLikes[`/users/${handle}/likedTweets/${tweetId}`] = true;

  return update(ref(db), updateLikes);
};

export const dislikeTweet = (handle, tweetId) => {
  const updateLikes = {};
  updateLikes[`/tweets/${tweetId}/likedBy/${handle}`] = null;
  updateLikes[`/users/${handle}/likedTweets/${tweetId}`] = null;

  return update(ref(db), updateLikes);
};

export const deleteTweet = async (id) => {
  const tweet = await getTweetById(id);
  const updateLikes = {};

  tweet.likedBy.forEach((handle) => {
    updateLikes[`/users/${handle}/likedTweets/${id}`] = null;
  });

  await update(ref(db), updateLikes);

  return update(ref(db), {
    [`/tweets/${id}`]: null,
  });
};

export const editPost = (tweetId, newContent, newTitle) => {
  const updateContent = {};
  updateContent[`/tweets/${tweetId}/content`] = newContent;
  updateContent[`/tweets/${tweetId}/title`] = newTitle;

  return update(ref(db), updateContent);
};

// export const likeTweet = (handle, tweetId) => {
//   const updateLikes = {};
//   updateLikes[`/tweets/${tweetId}/likedBy/${handle}`] = true;
//   updateLikes[`/users/${handle}/likedTweets/${tweetId}`] = true;

//   return update(ref(db), updateLikes);
// };

export const addComment = (content, handle, postId) => {
  return push(ref(db, `tweets/${postId}/comments`), {
    content,
    author: handle,
    createdOn: Date.now(),
  }).then((result) => {
    return getCommentById(postId, result.key);
  });
};
export const getCommentById = (postId, id) => {
  return get(ref(db, `tweets/${postId}/comments/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`comment with id ${id} does not exist!`);
    }

    const comment = result.val();
    comment.id = id;
    comment.createdOn = new Date(comment.createdOn);

    return comment;
  });
};

export const getPostComments = (postId) => {
  return get(ref(db, `tweets/${postId}/comments`)).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }
    const comments = snapshot.val();
    return Object.keys(comments).map((key) => {
      const comment = comments[key];
  
      return {
        ...comment,
        id: key,
      };
    })
  });
};


export const getLiveTweetById = (id, listen) => {
  return onValue(ref(db, `tweets/${id}`), listen);
};

export const deleteComment = (postId, id) => {
  return update(ref(db), {
    [`tweets/${postId}/comments/${id}`]: null,
  });
};
export const updateComment = (postId, id, content) => {
  return update(ref(db), {
    [`tweets/${postId}/comments/${id}/content`]: content,
  });
};