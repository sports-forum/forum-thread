import { get, set, ref, query, equalTo, orderByChild, update, onValue, onChildChanged } from 'firebase/database';
import { db } from '../config/firebase-config';
import { userRole } from '../common/user-role';

export const getUserByHandle = (handle) => {

  return get(ref(db, `users/${handle}`));
};

export const createUserHandle = (handle, firstName, lastName, uid, email) => {

  return set(ref(db, `users/${handle}`), { handle, firstName, lastName, uid, email, createdOn: new Date(), likedTweets: {}, role: userRole.BASIC });
};

export const getUserData = (uid) => {

  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const updateUserRole = (handle, role) => {
  return update(ref(db), {
    [`users/${handle}/role`]: role,
  });
};

export const updateUserProfilePicture = (handle, url) => {
  return update(ref(db), {
    [`users/${handle}/avatarUrl`]: url,
  });
};

export const getUserCount = () => {
  return get(ref(db, 'users'))
    .then(snapshot => {
      if (!snapshot.exists()) {
        return 0;
      }

      return Object.keys(snapshot.val()).length;
    });
}

export const getLiveUserDatas = (listen) => {
  return onChildChanged(ref(db, 'users'), () => {
    get(ref(db, 'users'))
    .then(listen);
  });
};

export const getUserDatas = () => {
  return get(ref(db, 'users'));
}
