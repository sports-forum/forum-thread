import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyCjFp8FgQqvmm7ngWsd8RT7VMCV1OPy-pU",
  authDomain: "not-twitter-31b78.firebaseapp.com",
  databaseURL: "https://not-twitter-31b78-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "not-twitter-31b78",
  storageBucket: "not-twitter-31b78.appspot.com",
  messagingSenderId: "395713670491",
  appId: "1:395713670491:web:46f721ec1ff60e26be3e7e"
};
export const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);
export const storage = getStorage(app);
