import './Home.css';
import { useLocation, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { getUserCount } from '../../services/users.services';
import { getAllTweets } from '../../services/tweets.service';
import { Card } from 'react-bootstrap';


const Home = () => {
  
  const location = useLocation();
  const navigate = useNavigate();
  const [userCount, setUserCount] = useState(-1);
  const [tweetCount, setTweetCount] = useState(-1);

  useEffect(() => {
      getUserCount()
        .then(setUserCount)
        .catch(console.error);

      getAllTweets()
        .then(tweets => setTweetCount(tweets.length))
        .catch(console.error);
    }, []);

  if (location.state?.from?.pathname) {
    //navigate(location.state.from.pathname);
  }

  return (
	<Card className='mt-4 mb-4'>
    <Card.Header className='h4'>Welcome to Sport Forum!</Card.Header>
    <Card.Body className='d-flex justify-content-around'>
		<Card bg='info' key='1' text='white' style={{ width: '30%' }} className="mb-2">
    		<Card.Header className='text-center h3'>Total Users</Card.Header>
    		<Card.Body>
      			<Card.Title className='text-center large-letters'>{ userCount }</Card.Title>
    		</Card.Body>
  		</Card>
		<Card bg='secondary' key='2' text='white' style={{ width: '30%' }} className="mb-2">
    		<Card.Header className='text-center h3'>Total Posts</Card.Header>
    		<Card.Body>
      			<Card.Title className='text-center large-letters'>{ tweetCount }</Card.Title>
    		</Card.Body>
  		</Card>
    </Card.Body>
    {/*<div className='Home'>
      <h1>Not Twitter</h1>
      <h3>User count: { userCount }</h3>
      <h3>Tweet count: { tweetCount }</h3>
      <p>Join and <em>tweet</em> with your friends.</p>
  </div>*/}
  </Card>
  )
};

export default Home;
