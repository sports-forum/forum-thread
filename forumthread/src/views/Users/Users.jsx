import './Users.css';
import { useState, useEffect, useContext } from 'react';
import AppContext from '../../providers/AppContext';
import User from '../../components/User/User';
import { getLiveUserDatas, getUserDatas, updateUserRole } from '../../services/users.services';
import { userRole } from '../../common/user-role';
import { Card, ListGroup } from 'react-bootstrap';

const Users = () => {
    const { userData } = useContext(AppContext);

    const [userDatas, setUserDatas] = useState([]);

    useEffect(() => {
        const listen = (snapshot) => {
            console.log('users changes detected');
            if (!snapshot.exists()) {
                setUserDatas([]);
            }
            setUserDatas(Object.keys(snapshot.val()).map(key => snapshot.val()[key]));

        }
        getUserDatas().then(listen);

        const unsubscribe = getLiveUserDatas(listen);

        return () => unsubscribe();
    }, []);

    const blockUser = (handle) => {
        if (handle === userData.handle) {
            alert("You can't block yourself");
            return;
        }
        updateUserRole(handle, userRole.BLOCKED);
    }
    const unblockUser = (handle) => {
        updateUserRole(handle, userRole.BASIC);
    }
    const makeAdmin = (handle) => {
        console.log("making admin" + handle);
        updateUserRole(handle, userRole.ADMIN);
    }

    return (
        <Card className='mt-4 mb-4 '>
            <ListGroup variant="flush">
                {userDatas.map(data => <ListGroup.Item key={data.uid}><User currentUserData={data} blockUser={blockUser} unblockUser={unblockUser} makeAdmin={makeAdmin} /></ListGroup.Item>)}
            </ListGroup>
        </Card>
    );
};

export default Users;
