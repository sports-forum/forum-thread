import { useContext, useEffect, useState } from 'react'
import AppContext from '../../providers/AppContext'
import { getTweetsByAuthor, getLikedTweets } from '../../services/tweets.service';

import { ref } from 'firebase/database';
import { ref as storageRef, uploadBytes, getDownloadURL } from 'firebase/storage';
import { storage } from '../../config/firebase-config';
import { updateUserProfilePicture } from '../../services/users.services';
import { v4 } from 'uuid';

export default function Profile() {
  const { user, userData, setContext } = useContext(AppContext);
  const handle = userData.handle;
  const [tweets, setTweets] = useState({ liked: [], own: [] });

  console.log(v4());

  useEffect(() => {
    Promise.all([getTweetsByAuthor(handle), getLikedTweets(handle)])
      .then(([own, liked]) => {
        setTweets({ own, liked });
      })
      .catch(console.error);
  }, [handle]);

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];

    if (!file) return alert(`Please select a file!`);

    const picture = storageRef(storage, `images/${handle}/avatar`);

    uploadBytes(picture, file)
      .then(snapshot => {
        return getDownloadURL(snapshot.ref)
          .then(url => {
            return updateUserProfilePicture(handle, url)
              .then(() => {
                setContext({
                  user,
                  userData: {
                    ...userData,
                    avatarUrl: url,
                  },
                });
              })
          });
      })
      .catch(console.error);
  };

  return (
    <div>
      { userData.avatarUrl ? <img src={userData.avatarUrl} alt="profile" /> : null }
      <form onSubmit={uploadPicture}>
        <input type="file" name="file"></input>
        <button type="submit">Submit</button>
      </form>
      <h2>Posts by you, count: {tweets.own.length}</h2>

      <h2>Posts you have liked, count: {tweets.liked.length}</h2>
    </div>
  )
}
