import CreateTweet from '../../components/CreateTweet/CreateTweet';
import Tweet from '../../components/Tweet/Tweet';
import './Tweets.css';
import { useState, useEffect, useContext } from 'react';
import { getAllTweets, fromTweetsDocument, addTweet, likeTweet, dislikeTweet, deleteTweet, getLiveTweets, editPost } from '../../services/tweets.service';
import AppContext from '../../providers/AppContext';
import { Button, Col, Row } from 'react-bootstrap';
import { userRole } from '../../common/user-role';

const Tweets = ({ like, dislike, edit, delTweet }) => {
  const [tweets, setTweets] = useState([]);
  const { userData } = useContext(AppContext);

  useEffect(() => {
    const unsubscribe = getLiveTweets((snapshot) => {
      console.log('changes detected')
      setTweets(fromTweetsDocument(snapshot).slice().sort((a, b) => b.createdOn - a.createdOn));
    });
    getAllTweets()
      .then((result) => setTweets(result.slice().sort((a, b) => b.createdOn - a.createdOn)))
      .then(tweet => {
        
      })
      .catch(console.error);
    
      

    return () => unsubscribe();
  }, []);



  const createTweet = (content, title) => {

    return addTweet(content, title, userData.handle);
  };


  return (
    <div className='container-fluid mt-4 mb-4'>
      {userData?.role >= userRole.BASIC && <CreateTweet onSubmit={createTweet} />}

      <Row className='mt-4'>
        <Col>
          <Button onClick={() => setTweets(tweets.slice().sort((a, b) => b.createdOn - a.createdOn))} className='btn-info'>Sort by newest</Button>
          <Button onClick={() => setTweets(tweets.slice().sort((a, b) => b.likedBy.length - a.likedBy.length))} className='btn-info ml-3'>Sort by likes</Button>
        </Col>
      </Row>

      {tweets.length === 0
        ? <p>No posts to show.</p>
        : tweets.map((tweet, key) => <Tweet key={key} tweet={tweet} deleteTweet={delTweet} like={like} dislike={dislike} edit={edit} />)
      }
    </div>
  );
};

export default Tweets;
