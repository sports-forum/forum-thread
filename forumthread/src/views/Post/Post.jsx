
import './Post.css';
import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { deleteTweet, editPost, addComment, getPostComments, getTweetById, getLiveTweetById, deleteComment, updateComment } from '../../services/tweets.service';
import AppContext from '../../providers/AppContext';
import Comment from '../../components/Comment/Comment';
import { useParams } from 'react-router-dom'
import AddComment from '../../components/AddComment/AddComment';
import Tweet from '../../components/Tweet/Tweet';

const Post = ({ like, dislike, edit, delTweet }) => {
    const { postId } = useParams();

    const [main, setMain] = useState(null);
    const [comments, setComments] = useState([]);
    const { userData: { handle } } = useContext(AppContext);
    useEffect(() => {
        getLiveTweetById(postId, (snapshot) => {
            if (!snapshot.exists()) {
                throw new Error(`Tweet with id ${postId} does not exist!`);
            }

            const post = snapshot.val();
            post.id = postId;
            post.createdOn = new Date(post.createdOn);
            if (!post.likedBy) {
                post.likedBy = [];
            } else {
                post.likedBy = Object.keys(post.likedBy);
            }

            setMain(post);
        })

        getTweetById(postId)
            .then(setMain)
            .catch(console.error);

        getPostComments(postId)
            .then(setComments)
            .catch(console.error);
    }, []);

    const delComment = (id) => {
        deleteComment(postId, id)
            .catch(console.error);

        getPostComments(postId)
            .then(setComments)
            .catch(console.error);
    };

    const createComment = (content) => {

        addComment(content, handle, postId);

        getPostComments(postId)
            .then(setComments)
            .catch(console.error);
    };
    const editComment = (content, commentId) => {

        updateComment(postId, commentId, content);

        getPostComments(postId)
            .then(setComments)
            .catch(console.error);
    }



    return (
        <div className='container-fluid mt-4 mb-4'>
            {main !== null && <Tweet tweet={main} deleteTweet={delTweet} like={like} dislike={dislike} edit={edit} />}
            {handle !== null && <AddComment onSubmit={createComment} />}


            {comments.length === 0
                ? <p>No comments.</p>
                : comments.slice().sort((a, b) => b.createdOn - a.createdOn).map((comment, key) => <Comment key={key} comment={comment} deleteComment={delComment} edit={editComment} />)
            }

        </div>
    );
};

export default Post;
